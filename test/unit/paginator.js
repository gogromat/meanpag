var assert                  = require("chai").assert
    ,   _                   = require('lodash')
    ,   Paginator           = require("../../src/paginator")
    ,   QueryBuilder        = require("../../src/queryBuilder")
    ;

describe("Paginator", function () {

    describe("#getDefaultFilter()", function () {
        it("should return object", function () {
            assert.isTrue(_.isObject(new Paginator().getDefaultFilter()));
        });
    });

    describe("#setDefaultFilter()", function () {

        it("should return correct filter settings", function () {
            var pageNumber = 18,
                filter = { page: pageNumber };
            assert.equal(pageNumber, new Paginator().setDefaultFilter(filter).getDefaultFilter().page);
        });
    });

    describe("#setFilter()", function () {
        it("should return correct filter settings", function () {
            var limit = 500,
                filter = { limit: limit };
            assert.equal(new Paginator().setFilter(filter).getLimit(), 500);
            assert.equal(new Paginator(filter).getLimit(), 500);
        });
    });

    describe("#getFilter()", function () {

        it("should return correct filter settings", function () {
            var limit = 500,
               filter = { limit: limit };
            assert.equal(limit, new Paginator(filter).getFilter().limit);
        });

        it("should return correct filter after updating the default filter", function () {
            var totalPages = 100,
                filter = { totalPages: totalPages };
            assert.equal( totalPages, new Paginator().setDefaultFilter(filter).getFilter().totalPages);
        });

        it("should return default query builder limit", function () {
            assert.equal(100, new Paginator().getLimit());
            assert.equal(100, new Paginator().getFilter().limit);
        });
    });


    describe("#getQueryBuilder", function () {
        it("should get the Query Builder object back", function () {
            assert.typeOf(new Paginator().getQueryBuilder(), "object");
            assert.instanceOf(new Paginator().getQueryBuilder(), QueryBuilder);
        });
    });

    describe("#setTotalItems()", function () {
        it("should properly set total items", function () {
            assert.equal(new Paginator().setTotal(10000).getTotal(), 10000);
            assert.equal(new Paginator().setTotal(10).getTotalItems(), 10);
            assert.equal(new Paginator().setTotalItems(-10).getTotal(), 10);
        });
    });

    describe("#clearFilter()", function () {
        it("should clear query limit", function () {
            var p = new Paginator();
            p.setLimit(15);
            assert.equal(15, p.getLimit());
            p.clearLimit();
            assert.equal(0, p.getLimit());
        });

        it("should clear query filter", function () {
            var p2 = new Paginator();
            p2.setLimit(60);
            p2.clearFilter();
            assert.equal(0, p2.getLimit());
            assert.equal(0, p2.getFilter().limit);
        });

        it("should clear logical queries", function () {
            var p = new Paginator()
               .or({age: 15})
               .or({name: "Alex"});
            p.clearFind();
            assert(_.isEqual({}, p.getFind()));
            console.log(p.getFilter());
            assert(_.isEqual({}, p.getFilter().find));
        });
    });

});