var assert                  = require("chai").assert
    ,   _                   = require('lodash')
    ,   QueryBuilder        = require("../../src/queryBuilder")
    ,   Helper              = require("../../src/helper")
    ;


describe("QueryBuilder", function () {

    describe("#find()", function () {
        it("should set find field", function () {
            var findExp = { age: 4 };
            assert.isTrue(_.isEqual(findExp, new QueryBuilder().findWhere(findExp).getFind()));
        });
        it("should set array find field", function () {
            var findExp1 = {age: 40},
                findExp2 = {name:"Lenny"},
                fullExp = _.extend(_.clone(findExp1), findExp2);
            assert.isTrue(_.isEqual(fullExp, new QueryBuilder().findWhere([findExp1, findExp2]).getFind()));
        });
        it("should set object find field", function () {
            var findExp = {age: 15, name: "Lucy"};
            assert.isTrue(_.isEqual(findExp, new QueryBuilder().findWhere(findExp).getFind()));
        });
    });

    describe("#sort()", function () {
        it("should set sort field", function () {
            var sortExp = {name: 1};
            assert.isTrue(_.isEqual(sortExp, new QueryBuilder().sort(sortExp).getSort()));
        });
    });

    describe("#sortAsc()", function () {
        it("should set sort field in ascending order", function () {
            assert.isTrue(_.isEqual({ "age" : 1}, new QueryBuilder().sortAsc("age").getSort()))
        });
    });

    describe("#sortDesc()", function () {
        it("should set sort field in descending order", function () {
            assert.isTrue(_.isEqual({ "age" : -1}, new QueryBuilder().sortDesc("age").getSort()))
        });
    });

    _.each(QueryBuilder.logicalOperators, function (operator) {
        describe("#" + operator + "()", function () {
            it("should set expression", function () {
                var expression = {name: "Boris"},
                    queryBuilder = new QueryBuilder();
                queryBuilder[operator](expression);
                assert.equal(expression, queryBuilder.getFind()["$" + operator][0]);
            });
            it("should set multi-expression", function () {
                var expression1 = { name: "First" },
                    expression2 = { name: "Second" },
                    queryBuilder = new QueryBuilder();
                queryBuilder[operator](expression1);
                queryBuilder[operator](expression2);
                assert.isTrue(
                    Helper.arraysEqualPreSort(
                        [expression1, expression2],
                        queryBuilder.getFind()["$" + operator]
                    ));
            });
        });
    });

    _.each(QueryBuilder.comparisonOperators["object"], function (operator) {
        describe("#" + operator + "()", function () {
            it("should set expression", function () {
                var expression = {};
                expression["$" + operator] = 7;
                assert.isTrue(_.isEqual(expression, QueryBuilder[operator](7)));
            });
        });
    });

    _.each(QueryBuilder.comparisonOperators["array"], function (operator) {
        describe("#" + operator + "()", function () {
            it("should set expression", function () {
                var expression = {};
                expression["$" + operator] = [1,2,3];
                assert.isTrue(_.isEqual(expression, QueryBuilder[operator](1,2,3)));
            });
        });
    });

    describe("#getAreEmptyFindExpressions()", function () {
        it("should be empty if no expression was given", function () {
            assert.isTrue(new QueryBuilder().getAreEmptyFindExpressions());
        });
        it("should not be empty if logical expression was set", function () {
            assert.isFalse(new QueryBuilder().and({ age: 20}).getAreEmptyFindExpressions());
        });
    });

    describe("#setFilter()", function () {
        it("should get similar filter back", function () {
            var newFilter = {
                find: { "$and" : {name: "Bob"}, "$or" : [{quantity: 20},{age:40}] },
                sort: { name: 1 }
            };
            var queryBuilder = new QueryBuilder().setFilter(newFilter);
            assert.isTrue(_.isEqual(newFilter.find["$or"], queryBuilder.getFind()["$or"]));
            assert.isTrue(_.isEqual([newFilter.find["$and"]], queryBuilder.getFind()["$and"]));
            assert.isTrue(_.isEqual(newFilter.sort, queryBuilder.getSort()));
        });
        it("Should get same filter back", function () {
            var newFilter = {
                find: { "$and": [{name: "Zack"}], "$not": [{age: 14}, {shoeSize: 100}], someCoolStuff: 14 },
                limit: 5,
                sort: { name: -1}
            };
            var queryBuilder = new QueryBuilder().setFilter(newFilter);
            assert.isTrue(_.isEqual(newFilter,       queryBuilder.getFilter()));
            assert.isTrue(_.isEqual(newFilter.find,  queryBuilder.getFind()));
            assert.isTrue(_.isEqual(newFilter.limit, queryBuilder.getLimit()));
            assert.isTrue(_.isEqual(newFilter.sort,  queryBuilder.getSort()));
        });
    });

    describe("#noLimit()", function () {
        it("should set limit to 0", function () {
            assert.equal(0, new QueryBuilder().noLimit().getLimit());
        });
    });

    describe("#getLimit()", function () {
        it("should be a 100 by default", function () {
            assert.equal(100, new QueryBuilder().getLimit());
        });
    });

    describe("#getFind()", function () {
        it("should return filter's find", function () {
            var orExp = {quantity: 20};
            var find = {"$or": [orExp]};
            assert.isTrue(_.isEqual(find, new QueryBuilder().or(orExp).getFind()));
        });
        it("should produce empty filter find if no expression was given", function () {
            assert.isTrue(_.isEqual({}, new QueryBuilder().getFind()));
        });
        it("should have same filter find", function () {
            var orExpression = { age: 40 };
            assert.isTrue(
                _.isEqual(
                    { "$or" : [orExpression] },
                    new QueryBuilder().or(orExpression).getFind()
                ));
        });
        it("Should have same filter find on multiple", function () {
            var norExpression = { name: "First"},
                notExpression1 = { age : 5},
                notExpression2 = { age : 4};
            assert.isTrue(
                _.isEqual(
                    {
                        "$nor": [norExpression],
                        "$not": [notExpression1, notExpression2]
                    },
                    new QueryBuilder().nor(norExpression).not(notExpression1).not(notExpression2).getFind()
                )
            );
        });

    });

    describe("#setFindFieldExpression()", function () {
        it("should set field expression", function () {
            var findExp = {age: 13};
            assert.isTrue(_.isEqual(findExp, new QueryBuilder().setFindFieldExpression("age", 13).getFind()));
        });
        it("should set field array expression", function () {
            var findExp = {
                "$in" : [1,2],
                "$nin": [3,4]
            };
            assert.isTrue(_.isEqual(
                { "age" : findExp}, new QueryBuilder().setFindFieldExpression("age", findExp).getFind()
            ));
        });
    });

    describe("#clearSort()", function () {
        it("should clear sort", function () {
            var qb = new QueryBuilder().setSort({name: 1}).clearSort();
            assert.isNull(qb.getSort());

            var qb2 = new QueryBuilder().setSort({name: 1, age: -1}).clearSort();
            assert.isNull(qb2.getSort());
        });
    });

    describe("#clearLimit()", function () {
        it("should clear limit", function () {
            var qb = new QueryBuilder().setLimit(15).clearLimit();
            assert.equal(0, qb.getLimit());
        });
    });

    describe("#clearFind()", function () {
        it("should clear find expressions", function () {
            var qb = new QueryBuilder()
                .or({name: "Alex", lastName: "A"})
                .and({age:15}).findWhere({wears: "hat", uses: "train"})
                .clearFind();
            assert.isTrue(_.isEmpty(qb.getFind()));
        });
    });

});