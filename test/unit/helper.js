var     assert  = require("chai").assert
    ,   _       = require("lodash")
    ,   Helper  = require("../../src/helper")
    ;

describe("Helper", function () {

    describe("#arraysEqual()", function () {

        it("arrays should be same", function () {
            assert.isTrue(Helper.arraysEqual([4],[4]));
            assert.isTrue(Helper.arraysEqual([{"a":4}], [{"a":4}]));
        });

        it("arrays should not be same", function () {
            assert.isFalse(Helper.arraysEqual([1,2],[2,1]));
            assert.isFalse(Helper.arraysEqual([1,2],[2,1,3]));
            assert.isFalse(Helper.arraysEqual([5],[6]));
            assert.isFalse(Helper.arraysEqual([1,2,4,3],[4,2,3,1]));
        });
    });

    describe("#arraysEqualPreSort()", function () {

        it("arrays should be same", function () {
            assert.isTrue(Helper.arraysEqualPreSort([1,2],[2,1]));
            assert.isTrue(Helper.arraysEqualPreSort([1,2,3,4],[4,3,2,1]));
            assert.isTrue(Helper.arraysEqualPreSort([1,2,4,3],[4,2,3,1]));
        });

        it("arrays should not be same", function () {
            assert.isFalse(Helper.arraysEqualPreSort([1,2,3],  [2,1]));
            assert.isFalse(Helper.arraysEqualPreSort([1,2,3,4],[5,3,2,1]));
        });
    });

    describe("#capitalize()", function () {
        it("should return capitalized string", function () {
            assert.equal("America", Helper.capitalize("america"));
            assert.equal("8", Helper.capitalize("8"));
            assert.equal("AAA", Helper.capitalize("AAA"));
            assert.equal("AAA", Helper.capitalize("aAA"));
        });
    });
});