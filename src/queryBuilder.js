var   _             = require("lodash")
    , Helper        = require("./helper")
    ;

(function () {
    var root = this;

    /**
     * Used to build-up mongoose queries as a JSON object
     *
     * @returns {QueryBuilder}
     * @constructor
     */
    var QueryBuilder = function () {

        var self = this;

        self.filter = {
            find: {},
            sort: {},
            limit: 100
        };

        self.allFilterKeys = _.keys(self.filter);

        _.extend(self.filter, {
            findExpressions: {},
            logicalQueryOperators: {}
        });
        _.each(QueryBuilder.logicalOperators, function (operator) {
            self.filter.logicalQueryOperators["$" + operator] = [];
        });

        return self;
    };

    QueryBuilder.logicalOperators = ["and", "or", "not", "nor"];

    QueryBuilder.comparisonOperators = {
        "object" : ["gt", "gte", "lt", "lte", "ne"],
        "array"  : ["in", "nin"],
        "regex"  : ["regex"]
    };

    QueryBuilder.publicAPI =
        QueryBuilder.logicalOperators
            .concat(QueryBuilder.comparisonOperators["object"])
            .concat(QueryBuilder.comparisonOperators["array"])
            .concat(QueryBuilder.comparisonOperators["regex"])
            .concat([
                "getFilter", "setFilter", "filter", "clearFilter",
                "getFind", "setFind", "find", "findWhere", "setFindFieldExpression", "getAreEmptyFindExpressions", "clearFind",
                "getSort", "setSort", "sort", "sortAsc", "sortDesc", "clearSort",
                "getLimit", "setLimit", "limit", "noLimit", "clearLimit"
            ]);

    _.each(QueryBuilder.logicalOperators, function (operator) {
        QueryBuilder.prototype[operator] = function (expression, replace) {
            var self = this;
            if (_.isObject(expression)) {
                if (_.isArray(expression)) {
                    self.filter.logicalQueryOperators["$" + operator] =
                        self.filter.logicalQueryOperators["$" + operator].concat(expression);
                } else {
                    self.filter.logicalQueryOperators["$" + operator].push(expression);
                }
            }
            return this;
        };
    });

    _.each(QueryBuilder.comparisonOperators["object"], function (operator) {
        QueryBuilder[operator] = function (expression) {
            return Helper.getObjectWithKeySetTo("$" + operator, expression);
        };
    });

    _.each(QueryBuilder.comparisonOperators["array"], function (operator) {
        QueryBuilder[operator] = function () {
            return Helper.getObjectWithKeySetTo("$" + operator, Array.prototype.slice.call(arguments, 0));
        };
    });

    _.each(QueryBuilder.comparisonOperators["regex"], function (operator) {
        QueryBuilder[operator] = function (expression, options) {
            return {
                "$regex": expression,
                '$options': options
            };
        };
    });

    QueryBuilder.prototype.setFilter = QueryBuilder.prototype.filter = function (userFilter) {
        var self = this;
        _.each(_.pick(userFilter, self.allFilterKeys), function (value, key) {
            self["set" + Helper.capitalize(key)](value); // setLimit, setSort, setFind, etc.
        });
        return this;
    };

    QueryBuilder.prototype.clearFilter = function () {
        return this.clearLimit().clearSort().clearFind();
    };

    QueryBuilder.prototype.clearLimit = function () {
        return this.noLimit();
    };

    QueryBuilder.prototype.clearSort = function () {
        this.filter.sort = null;
        return this;
    };

    /**
     * filter.find is built from logicalQueryOperators and findExpressions
     * @returns {QueryBuilder}
     */
    QueryBuilder.prototype.clearFind = function () {
        this.filter.find = {};
        this.filter.findExpressions = {};
        _.each(this.filter.logicalQueryOperators, function (v, k, a) {
            a[k] = [];
        });
        return this;
    };

    QueryBuilder.prototype.setFind = QueryBuilder.prototype.find = function (expressions, replace) {
        var self = this;
        // $and, $or, name, age, etc
        _.each(expressions, function (value, key) {
            var setOfLogicalFindKeyFunctions = key.substr(1);
            if (_.indexOf(QueryBuilder.logicalOperators, setOfLogicalFindKeyFunctions) >= 0) {
                self[setOfLogicalFindKeyFunctions](value, replace); // $and, $or, etc
            } else {
                self.findWhere(Helper.getObjectWithKeySetTo(key, value, replace));
            }
        });
        return this;
    };

    // For non-logical expressions, like { name: "Bob" }
    QueryBuilder.prototype.findWhere = function (fieldsExpressions) {
        var self = this;
        if (_.isPlainObject(fieldsExpressions)) {
            for (var field in fieldsExpressions) {
                if (fieldsExpressions.hasOwnProperty(field)) {
                    self.setFindFieldExpression(field, fieldsExpressions[field]);
                }
            }
        }
        if (_.isArray(fieldsExpressions)) {
            fieldsExpressions.forEach(function (expression) {
                for (var field in expression) {
                    if (expression.hasOwnProperty(field)) {
                        self.setFindFieldExpression(field, expression[field]);
                    }
                }
            });
        }
        return this;
    };

    QueryBuilder.prototype.setFindFieldExpression = function (field, expression) {
        if (_.isObject(expression)) {
            if (_.isEmpty(this.filter.findExpressions[field])) {
                this.filter.findExpressions[field] = {};
            }
            _.extend(this.filter.findExpressions[field], expression);
        } else {
            this.filter.findExpressions[field] = expression;
        }
        return this;
    };

    QueryBuilder.prototype.getAreEmptyFindExpressions = function () {
        var self = this,
            allEmpty = true,
            operExp = null;

        if (!_.isEmpty(this.filter.findExpressions)) {
            allEmpty = false;
        }
        for (var operator in self.filter.logicalQueryOperators) {
            if (self.filter.logicalQueryOperators.hasOwnProperty(operator)) {
                operExp = self.filter.logicalQueryOperators[operator];
                if (!_.isEmpty(operExp)) {
                    allEmpty = false;
                }
            }
        }
        return allEmpty;
    };

    QueryBuilder.prototype.getFilter = function () {
        return {
            find: this.getFind(),
            sort: this.getSort(),
            limit: this.getLimit()
        };
    };

    QueryBuilder.prototype.getFind = function () {

        var self = this,
            operExp = null;

        if (self.getAreEmptyFindExpressions()) {
            // no query expressions were provided
            self.filter.find = {};
        } else {

            for (var operator in self.filter.logicalQueryOperators) {
                if (self.filter.logicalQueryOperators.hasOwnProperty(operator)) {
                    operExp = self.filter.logicalQueryOperators[operator];
                    if (!_.isEmpty(operExp)) {
                        self.filter.find[operator] = operExp;
                    }
                }
            }
            if (!_.isEmpty(self.filter.findExpressions)) {
                for (var val in self.filter.findExpressions) {
                    if (self.filter.findExpressions.hasOwnProperty(val)) {
                        self.filter.find[val] = self.filter.findExpressions[val];
                    }
                }
            }
        }
        return self.filter.find;
    };

    QueryBuilder.prototype.getSort = function () {
        return this.filter.sort;
    };

    QueryBuilder.prototype.setSort = QueryBuilder.prototype.sort = function (fieldExpressions) {
        _.extend(this.filter.sort, fieldExpressions);
        return this;
    };

    QueryBuilder.prototype.sortAsc = function (field) {
        _.extend(this.filter.sort, Helper.getObjectWithKeySetTo(field, 1));
        return this;
    };

    QueryBuilder.prototype.sortDesc = function (field) {
        _.extend(this.filter.sort, Helper.getObjectWithKeySetTo(field, -1));
        return this;
    };

    QueryBuilder.prototype.getLimit = function () {
        return this.filter.limit;
    };

    QueryBuilder.prototype.setLimit = QueryBuilder.prototype.limit = function (limit) {
        this.filter.limit = limit;
        return this;
    };

    QueryBuilder.prototype.noLimit = function () {
        this.filter.limit = 0;
        return this;
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = QueryBuilder;
        }
        exports.QueryBuilder = QueryBuilder;
    } else {
        root.QueryBuilder = QueryBuilder;
    }

}).call(this, Helper);