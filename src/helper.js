var _ = require("lodash");

(function (_) {

    var root = this;
    var Helper = {
        getObjectWithKeySetTo : function (key, val) {
            var obj = {};
            obj[key] = val;
            return obj;
        },
        arraysEqualPreSort : function (a, b) {
            return Helper.arraysEqual(a, b, function () {
                return [a.sort(), b.sort()];
            });
        },
        arraysEqual : function (a, b, modifyArrays) {
            if (a === b) return true;
            if (a == null || b == null) return false;
            if (a.length != b.length) return false;

            if (modifyArrays !== undefined) {
                // If you don't care about the order of the elements inside
                // the array, you should sort both arrays here.
                var modifiedArrays = modifyArrays(a, b);
                a = modifiedArrays[0]; b = modifiedArrays[1];
            }

            for (var i = 0; i < a.length; ++i) {
                if (a[i] !== b[i] && !_.isEqual(a[i], b[i])) return false;
            }
            return true;
        },
        capitalize: function (string) {
            return string.substr(0, 1).toUpperCase() + string.substr(1);
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = Helper;
        }
        exports.Helper = Helper;
    } else {
        root.Helper = Helper;
    }

}).call(this, _);