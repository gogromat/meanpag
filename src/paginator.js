var _               = require("lodash")
    , QueryBuilder  = require("./queryBuilder")
    ;

(function (_, QueryBuilder) {

    var root = this,
        defaultPaginationFilter = {
            totalPages      :    1,
            // Current page we are on
            page            :    1,
            // Items that are being displayed on this page
            items           :    0,
            // Total # of records in the database that satisfy filter
            totalItems      :    0
        },
        paginationFilterKeys = _.keys(defaultPaginationFilter);

    var Paginator = function (userFilter, queryBuilder) {

        // Set default pagination filter
        this.filter = _.clone(defaultPaginationFilter, true);

        // Set new query builder
        this.mongoBuilder = queryBuilder || new QueryBuilder();

        // Set new user-defined filter
        this.setFilter(userFilter);

        return this;
    };

    Paginator.prototype.setTotal = Paginator.prototype.setTotalItems = function (itemsCount) {
        this.filter.totalItems = Math.abs(parseInt(itemsCount, 10));
        return this;
    };

    Paginator.prototype.getTotal = Paginator.prototype.getTotalItems = function () {
        return this.filter.totalItems;
    };

    Paginator.prototype.setTotalPages = function (pages) {
        if (pages) {
            this.filter.totalPages = Math.abs(parseInt(pages, 10));
        } else {
            this.filter.totalPages = Math.ceil(this.filter.totalItems / this.filter.limit);
        }
        if (!this.filter.totalPages) {
            this.filter.totalPages = 1;
        }
        return this;
    };

    Paginator.prototype.getTotalPages = function () {
        return this.filter.totalPages;
    };

    Paginator.prototype.setPage = Paginator.prototype.setPageNumber = function (pageNumber) {
        this.filter.page = parseInt(pageNumber, 10);
        return this;
    };

    Paginator.prototype.getPage = Paginator.prototype.getPageNumber = function () {
        return this.filter.page;
    };

    Paginator.prototype.getSkip = function () {
        return this.filter.skip;
    };

    Paginator.prototype.setupFilter = function () {
        if (this.filter.page <= 1) {
            // Cannot skip records if at first page
            this.filter.skip = 0;
        } else {
            // User chose other pages. Skip x "limit" records.
            this.filter.skip = (this.filter.page-1) * this.filter.limit;
        }

        var itemsRemaining = this.filter.totalItems - this.filter.skip;

        if (itemsRemaining <= 0) {
            // Skipped too many records, go back to the first page
            // TODO: setting to go to specific page...maybe last page?
            this.filter.page  = 1;
            this.filter.skip  = 0;
            this.filter.limit = 100;
            this.setPageItems();
        } else if (itemsRemaining > this.filter.limit) {
            this.filter.items = this.filter.limit;
        } else {
            // [1,limit] items remain (last page)
            this.filter.items = itemsRemaining;
        }

        this.setTotalPages();
    };

    Paginator.prototype.setPageItems = function () {
        this.filter.items = this.filter.totalItems > this.filter.limit ? this.filter.limit : this.filter.totalItems
    };

    _.each(QueryBuilder.publicAPI, function (operator) {
        Paginator.prototype[operator] = function () {
            var isGettingInternalValue = (operator.substr(0, 3) == "get"),
                returnValue = this.mongoBuilder[operator].apply(this.mongoBuilder, arguments);
            return (isGettingInternalValue ? returnValue : this);
        };
    });

    Paginator.prototype.setFilter = function (userFilter) {
        _.extend(this.filter, this.mongoBuilder.setFilter(userFilter).getFilter());
        _.extend(this.filter, _.pick(userFilter, paginationFilterKeys));
        return this;
    };

    Paginator.prototype._extendFilterWithDefaultFilter = function () {
        _.extend(this.filter, _.clone(defaultPaginationFilter, true));
    };

    Paginator.prototype.getQueryBuilder = function () {
        return this.mongoBuilder;
    };

    Paginator.prototype.getDefaultFilter = function () {
        return defaultPaginationFilter;
    };

    Paginator.prototype.setDefaultFilter = function (newDefaultFilter) {
        _.extend(defaultPaginationFilter, newDefaultFilter);
        this._extendFilterWithDefaultFilter();
        return this;
    };

    Paginator.prototype.getFilter = function () {
        _.extend(this.filter, this.mongoBuilder.getFilter());
        return this.filter;
    };

    Paginator.prototype.setAllFilterParams = function () {
        this.setFilterFindParams();
        this.setFilterSortParams();
        return this;
    };

    Paginator.prototype.setFilterFind = function () {
        this.filter.find = this.mongoBuilder.getFind();
        return this;
    };

    Paginator.prototype.setFilterSort = function () {
        this.filter.sort = this.mongoBuilder.getSort();
        return this;
    };


    /**
     *
     * @param mongooseCollection
     * @param errorCb
     * @param successCb
     */
    Paginator.prototype.filterMongooseCollection = function (mongooseCollection, errorCb, successCb) {

        var self = this;

        mongooseCollection.count(self.getFind(), function countCallback (err, count) {
            if (err) {
                return errorCb(err, count);
            } else {
                return findObjectsWithCount(mongooseCollection, count);
            }
        });

        function findObjectsWithCount (mongooseCollection, count) {

            self.setTotalItems(count);
            self.setupFilter();

            if (count == 0) {
                return returnValues(null, []);
            }

            mongooseCollection
                .find(self.getFind())
                .sort(self.getSort())
                .skip(self.getSkip())
                .limit(self.getLimit())
                .lean()
                .exec('find', returnValues);

            function returnValues (err, values) {
                return (err) ? errorCb(err, values) : successCb(values);
            }
        }
    };

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = Paginator;
        }
        exports.Paginator = Paginator;
    } else {
        root.Paginator = Paginator;
    }

}).call(this, _, QueryBuilder);