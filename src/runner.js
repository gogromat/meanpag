var Paginator    = require("./paginator"),
    QueryBuilder = require("./queryBuilder");
global.MEANPag = {
    "Paginator"     : Paginator,
    "QueryBuilder"  : QueryBuilder
};